## Project Setup

```
flutter pub global activate get_cli
get create project:"my cool project"

```
## Create view in Clean architecture 

```
get create screen:home 
```

## Create view in Getx pattern 
```
get create page:home
```

## Add git in project

```
git init
git add .
git commit -m "initial setup"
git remote add origin https://gitlab.com/sadia.nagorik/flutter-documentation.git
git branch -M main
git push -uf origin main
```
